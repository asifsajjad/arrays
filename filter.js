function filter(elements=[],cb)
{
    let res=[];

    if(!elements || !cb)
    {
        return [];
    }

    for(let index=0;index<elements.length;index++)
    {
        if(cb(elements[index],index,elements)===true)
        {
            res.push(elements[index]);
        }
    }
    return res;
}


module.exports=filter;