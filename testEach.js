const each = require('./each');
const items = [1, 2, 3, 4, 5, 5];

function testEach(userArray, userFunction) {
  each(userArray, userFunction);
}

function simpleLog(input) {
  console.log(input);
}


testEach(items, simpleLog);
testEach([], simpleLog);
testEach(items);
testEach();
