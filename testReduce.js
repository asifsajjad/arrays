const reduce = require('./reduce');

function testReduce(userArr, userFunction, startingValue) {
    return reduce(userArr, userFunction, startingValue);
}

const items = [1, 2, 3, 4, 5, 5];

function abc(a, b) {
    return a + b;
}

console.log(testReduce(items, abc));
console.log(testReduce(items, abc, 5));
console.log(testReduce([], abc));
console.log(testReduce(items));
console.log(testReduce());