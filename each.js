function each(inputArr=[], userFunction)
{
    if(!inputArr|| !userFunction)
    {
        return;
    }
    for(let index=0;index<inputArr.length;index++)
    {
        let outArgument={
            value:null,
            id: null
        };
        outArgument.id=index;
        outArgument.value=inputArr[index];
        userFunction(outArgument.value,outArgument.id,inputArr);
    }
    return;
}

module.exports=each;

// const items = [1, 2, 3, 4, 5, 5];
// each(items,(item)=>{ console.log(item);});