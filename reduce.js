function reduce(elements = [], cb, startingValue) {
    if (!elements || !cb) {
        return;
    }
    let accumulator = elements[0];
    let startingElement = 1;
    if (startingValue) {
        accumulator = startingValue;
        startingElement = 0;
    }
    for (let index = startingElement; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index], index, elements);
    }
    return accumulator;
}

module.exports = reduce;