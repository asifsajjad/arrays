function find(elements = [], cb) {
    if (!elements || !cb) {
        return;
    }
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements)===true) {
            return elements[index];
        }
    }
    return;
}


module.exports = find;

