

function map(inputArr = [], inputFunction) {


    if (!inputArr || !inputFunction) {
        return [];
    }
    let res = [];

    for (let index = 0; index < inputArr.length; index++) {

        if (inputArr[index] === undefined) {
            res.push(undefined);
        }
        else {
            res.push(inputFunction(inputArr[index], index, inputArr));
        }
    }

    return res;
}

module.exports = map;