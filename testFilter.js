const filter = require('./filter');

function testFilter(userArr, userFunction) {
    return filter(userArr, userFunction);
}


const items = [1, 2, 3, 4, 5, 5];

function test(item) {
    return item > 4;
}

console.log(testFilter(items, test));
console.log(testFilter(items));
console.log(testFilter([], test));
console.log(testFilter());
