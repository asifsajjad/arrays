const flatten = require('./flatten');

function testFlatten(userArr, userDepth, resPlaceholder) {
    return flatten(userArr, userDepth, resPlaceholder);
}

const nestedArray = [1, [2], [[3]], [[[4]]],[[[[5],6]]]];
const nestedArray1 = [1, [2], [[3]], [[[4]]]];
const nestedArray2 = [1, 2, , 3, 4];

console.log(testFlatten(nestedArray, 2));
console.log(testFlatten(nestedArray));
console.log(testFlatten(nestedArray1));
console.log(testFlatten(nestedArray2));
console.log(testFlatten());
console.log(testFlatten([]));

// let firstArr1 = testFlatten(nestedArray,2);
// let secondArr1 = nestedArray.flat(2);
// let isEqual1 = firstArr1.toString() === secondArr1.toString();
// console.log(isEqual1);
