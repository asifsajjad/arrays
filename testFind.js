const find = require('./find');

function testFind(userArr, userFunction) {
    return find(userArr, userFunction);
}

const items = [1, 2, 3, 4, 5, 5];
function abc(x) {
    return x === 3;
}

console.log(testFind(items, abc));
console.log(testFind([], abc));
console.log(testFind(items));
console.log(testFind());
console.log(testFind('', abc));
