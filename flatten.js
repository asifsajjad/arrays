
function flatten(elements, depth, res) {
    res = res || [];
    if (depth === undefined || depth === null) {
        depth = 1;
    }

    // console.log('res',res);
    // console.log('depth',depth);

    if (!elements) {
        return res;
    }
    else {

        for (let index = 0; index < elements.length; index++) {
            // console.log('index: '+index);
            if (Array.isArray(elements[index])) {

                if (depth === 0) {
                    res.push(elements[index]);
                    // console.log('depth is zeroed so pushing: ',elements[index]);
                }
                else {
                    // console.log('still an array, so diving in: ',elements[index]);
                    // console.log(' res: '+res);
                    flatten(elements[index], depth - 1, res);

                }

            }
            else {
                if (!elements[index]) {
                    continue;
                }
                res.push(elements[index]);
                // console.log('Not an array, pushing: ',elements[index]);
            }

        }
    }
    // console.log('res at end: ',res);

    return res;
}



module.exports = flatten;