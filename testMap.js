const map = require('./map');

function testMap(userArr, userFunction) {
    return map(userArr, userFunction);
}


const items = [1, 2, 3, 4, 5, 5];
function square(num) {
    return num * num;
}

console.log(testMap(items, square));
console.log(testMap());
console.log(testMap(items));
console.log(testMap([], square));
console.log(testMap(items, parseInt));
console.log(testMap(items, parseFloat));
